const express = require("express");
const passport = require("passport");
const router = express.Router();

// Auth with google
// Get req. to '/auth/google'
router.get("/google", passport.authenticate("google", { scope: ["profile"] }));

// google auth callback
// Get req. to '/auth/google/callback'
router.get(
  "/google/callback",
  passport.authenticate("google", { failureRedirect: "/" }),
  (req, res) => {
    res.redirect("/dashboard");
  }
);

// Logout user
// route /auth/logout
router.get("/logout", (req, res) => {
  req.logout();
  res.redirect("/");
});

module.exports = router;
